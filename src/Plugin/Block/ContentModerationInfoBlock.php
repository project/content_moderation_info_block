<?php

namespace Drupal\content_moderation_info_block\Plugin\Block;

use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\content_moderation_info_block\Form\ContentModerationInfoBlockModerationForm;
use Drupal\content_moderation_info_block\Form\ContentModerationInfoBlockPublishForm;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\TranslatableRevisionableInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a content moderation info block.
 *
 * @Block(
 *   id = "content_moderation_info_block",
 *   admin_label = @Translation("Moderation info"),
 *   category = @Translation("Content moderation"),
 *   deriver = "\Drupal\content_moderation_info_block\Plugin\Derivative\ContentModerationInfoBlockDeriver",
 * )
 */
class ContentModerationInfoBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The moderation information service.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface
   */
  protected $moderationInfo;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Constructs a new ContentModerationInfoBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\content_moderation\ModerationInformationInterface $moderation_info
   *   The moderation information service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ModerationInformationInterface $moderation_info, FormBuilderInterface $form_builder, DateFormatterInterface $date_formatter, EntityRepositoryInterface $entity_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->moderationInfo = $moderation_info;
    $this->formBuilder = $form_builder;
    $this->dateFormatter = $date_formatter;
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('content_moderation.moderation_information'),
      $container->get('form_builder'),
      $container->get('date.formatter'),
      $container->get('entity.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'changed_date_display' => TRUE,
      'author_display' => TRUE,
      'latest_revision_display' => TRUE,
      'current_state_display' => TRUE,
      'change_state_display' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $configuration = $this->getConfiguration();

    $form['changed_date_display'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display changed date'),
      '#default_value' => $configuration['changed_date_display'],
    ];

    $form['author_display'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display author'),
      '#default_value' => $configuration['author_display'],
    ];

    $form['latest_revision_display'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display latest revision'),
      '#default_value' => $configuration['latest_revision_display'],
    ];

    $form['current_state_display'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display current state'),
      '#default_value' => $configuration['current_state_display'],
    ];

    $form['change_state_display'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display change state form'),
      '#default_value' => $configuration['change_state_display'],
    ];

    $form['revision_log_message_input_display'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display revision log message input field in change state form'),
      '#default_value' => $configuration['revision_log_message_input_display'],
      '#states' => [
        'visible' => [
          ':input[name="settings[change_state_display]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['changed_date_display'] = $form_state->getValue('changed_date_display');
    $this->configuration['author_display'] = $form_state->getValue('author_display');
    $this->configuration['latest_revision_display'] = $form_state->getValue('latest_revision_display');
    $this->configuration['current_state_display'] = $form_state->getValue('current_state_display');
    $this->configuration['change_state_display'] = $form_state->getValue('change_state_display');
    $this->configuration['revision_log_message_input_display'] = $form_state->getValue('revision_log_message_input_display');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $configuration = $this->getConfiguration();
    $entity = $this->getContextValue('entity');

    if ($configuration['changed_date_display']) {
      $changed_date = $this->getChangedDateText($entity);
      if ($changed_date) {
        $build['changed_date'] = [
          '#type' => 'item',
          '#title' => $this->t('Last saved on'),
          '#markup' => $changed_date,
        ];
      }
    }

    if ($configuration['author_display']) {
      $author = $this->getAuthorText($entity);
      if ($author) {
        $build['author'] = [
          '#type' => 'item',
          '#title' => $this->t('Last saved by'),
          '#markup' => $author,
        ];
      }
    }

    if ($configuration['latest_revision_display']) {
      if ($entity instanceof TranslatableRevisionableInterface) {
        $active_entity = $this
          ->entityRepository
          ->getActive($entity->getEntityTypeId(), $entity->id());

        $is_latest_revision = $active_entity->getRevisionId() === $entity->getRevisionId();
      }
      else {
        $is_latest_revision = $entity->isLatestRevision();
      }

      $build['latest_revision'] = [
        '#type' => 'item',
        '#title' => $this->t('Is latest revision'),
        '#markup' => $is_latest_revision ? $this->t('Yes') : $this->t('No'),
      ];
    }

    if ($configuration['current_state_display']) {
      $current_state = $this->getCurrentStateText($entity);
      if ($current_state) {
        $build['current_state'] = [
          '#type' => 'item',
          '#title' => $this->t('Current state'),
          '#markup' => $current_state,
        ];
      }
    }

    if ($configuration['change_state_display']) {
      $class = NULL;
      if ($this->moderationInfo->isModeratedEntity($entity)) {
        $class = ContentModerationInfoBlockModerationForm::class;
      }
      elseif ($entity instanceof EntityPublishedInterface) {
        $class = ContentModerationInfoBlockPublishForm::class;
      }

      if ($class) {
        $build['change_state'] = $this
          ->formBuilder
          ->getForm($class, $entity, $configuration['revision_log_message_input_display']);
      }
    }

    return $build;
  }

  /**
   * Get the changed date text for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity object.
   *
   * @return string|null
   *   A string or NULL if no changed date.
   */
  protected function getChangedDateText(EntityInterface $entity) {
    if ($entity->isNew()) {
      return $this->t('Not saved yet');
    }

    if ($entity instanceof EntityChangedInterface) {
      return $this->dateFormatter->format($entity->getChangedTime(), 'short');
    }

    return NULL;
  }

  /**
   * Get the author text for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity object.
   *
   * @return string|null
   *   A string or NULL if no author.
   */
  protected function getAuthorText(EntityInterface $entity) {
    if ($entity->isNew()) {
      return $this->t('Not saved yet');
    }

    if ($entity instanceof RevisionLogInterface) {
      return $entity->getRevisionUser()
        ? $entity->getRevisionUser()->getDisplayName()
        : NULL;
    }

    if ($entity instanceof EntityOwnerInterface) {
      return $entity->getOwner()
        ? $entity->getOwner()->getDisplayName()
        : NULL;
    }

    return NULL;
  }

  /**
   * Get the current state text for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity object.
   *
   * @return string|null
   *   A string or NULL if no changed date.
   *
   * @throws \InvalidArgumentException
   */
  protected function getCurrentStateText(EntityInterface $entity) {
    if (!$entity instanceof ContentEntityInterface) {
      return NULL;
    }

    if ($this->moderationInfo->isModeratedEntity($entity)) {
      $current_state = $entity->moderation_state->value;
      $workflow = $this->moderationInfo->getWorkflowForEntity($entity);
      if ($current_state && $workflow) {
        return $workflow->getTypePlugin()->getState($current_state)->label();
      }
    }

    if ($entity instanceof EntityPublishedInterface) {
      return $entity->isPublished()
        ? $this->t('Published')
        : $this->t('Unpublished');
    }

    return NULL;
  }

}
