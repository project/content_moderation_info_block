<?php

namespace Drupal\content_moderation_info_block\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides block plugin definitions for content moderation blocks.
 *
 * @see \Drupal\content_moderation_info_block\Plugin\Block\ContentModerationInfoBlock
 */
class ContentModerationInfoBlockDeriver extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The base plugin ID.
   *
   * @var string
   */
  protected $basePluginId;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * The moderation information service.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface
   */
  protected $moderationInfo;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('content_moderation.moderation_information'),
      $container->get('string_translation')
    );
  }

  /**
   * Constructs a ContentModerationInfoBlock object.
   *
   * @param string $base_plugin_id
   *   The base plugin ID.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The bundle info service.
   * @param \Drupal\content_moderation\ModerationInformationInterface $moderation_info
   *   The moderation information service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $bundle_info, ModerationInformationInterface $moderation_info, TranslationInterface $string_translation) {
    $this->basePluginId = $base_plugin_id;
    $this->entityTypeManager = $entity_type_manager;
    $this->bundleInfo = $bundle_info;
    $this->moderationInfo = $moderation_info;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $entity_types = $this->entityTypeManager->getDefinitions();

    foreach ($entity_types as $entity_type) {
      if ($this->moderationInfo->isModeratedEntityType($entity_type)) {
        $derivative = $base_plugin_definition;
        $derivative['admin_label'] = $this->t('Moderation info (@entity_type)', [
          '@entity_type' => $entity_type->getLabel(),
        ]);

        $entity_context_definition = EntityContextDefinition::fromEntityType($entity_type)
          ->setLabel($entity_type->getLabel());

        // Limit this entity type specific block to bundles that can be
        // moderated.
        $entity_type_bundle_ids = array_keys($this->bundleInfo->getBundleInfo($entity_type->id()));
        $bundle_ids = array_filter($entity_type_bundle_ids, function ($bundle_id) use ($entity_type) {
          return $this->moderationInfo->shouldModerateEntitiesOfBundle($entity_type, $bundle_id);
        });
        if (!empty($bundle_ids)) {
          $entity_context_definition->addConstraint('Bundle', $bundle_ids);
        }

        $derivative['context_definitions'] = [
          'entity' => $entity_context_definition,
        ];

        $this->derivatives[$entity_type->id()] = $derivative;
      }
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
