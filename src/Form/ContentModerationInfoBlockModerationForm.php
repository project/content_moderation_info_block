<?php

namespace Drupal\content_moderation_info_block\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\content_moderation\Form\EntityModerationForm;
use Drupal\content_moderation\StateTransitionValidationInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple UI for changing moderation state.
 */
class ContentModerationInfoBlockModerationForm extends EntityModerationForm {

  /**
   * The moderation state transition validation service.
   *
   * @var \Drupal\content_moderation\StateTransitionValidationInterface
   */
  protected $validation;

  /**
   * Constructs a ContentModerationInfoBlockModerationForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\content_moderation\StateTransitionValidationInterface $validation
   *   The moderation state transition validation service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager, StateTransitionValidationInterface $validation) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->setModuleHandler($module_handler);
    $this->setEntityTypeManager($entity_type_manager);
    $this->validation = $validation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
      $container->get('content_moderation.state_transition_validation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_moderation_info_block_form';
  }

  /**
   * Form constructor.
   *
   * This form is the same as the original entity moderation form
   * ('content_moderation_entity_moderation_form'), with a few minor
   * alterations.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
   *   (optional) The entity object to display the form for.
   * @param bool $revision_log_message
   *   (optional) If TRUE, a revision log message can be input. Defaults to
   *   TRUE.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, ContentEntityInterface $entity = NULL, $revision_log_message = TRUE) {
    if (!$entity) {
      return $form;
    }

    $this->setEntity($entity);
    $this->setOperation('moderate');
    $form_state->set('entity', $entity);
    $form = parent::buildForm($form, $form_state);

    $form['current']['#access'] = FALSE;

    $form['new_state']['widget'][0]['state']['#title'] = $this->t('Action');

    // Use the transition labels in the options instead of the state labels.
    /** @var \Drupal\workflows\Transition[] $transitions */
    $transitions = $this->validation->getValidTransitions($entity, $this->currentUser());
    $current_state = $entity->moderation_state ? $entity->moderation_state->value : NULL;
    foreach ($transitions as $transition) {
      $target_state_id = $transition->to()->id();
      // Don't display state if it's target is equal to current state.
      if ($current_state === $target_state_id) {
        unset($form['new_state']['widget'][0]['state']['#options'][$target_state_id]);
      }
      // Replace target state label with transition label.
      elseif (!empty($form['new_state']['widget'][0]['state']['#options'][$target_state_id])) {
        $form['new_state']['widget'][0]['state']['#options'][$target_state_id] = $transition->label();
      }
    }

    $form['revision_log']['#access'] = $revision_log_message;

    unset($form['#theme']);

    if (!empty($form['#attached']['library'])) {
      $key = array_search('content_moderation/content_moderation', $form['#attached']['library'], TRUE);
      if ($key !== FALSE) {
        unset($form['#attached']['library'][$key]);
      }
    }

    return $form;
  }

}
