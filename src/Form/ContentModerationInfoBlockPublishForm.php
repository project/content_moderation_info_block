<?php

namespace Drupal\content_moderation_info_block\Form;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a simple UI for changing the published state.
 */
class ContentModerationInfoBlockPublishForm extends FormBase {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\Core\Entity\EntityPublishedInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return $this->getFormId();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_moderation_info_block_entity_publish_form';
  }

  /**
   * Gets the form entity.
   *
   * @return \Drupal\Core\Entity\EntityPublishedInterface
   *   The current form entity.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Sets the form entity.
   *
   * @param \Drupal\Core\Entity\EntityPublishedInterface $entity
   *   The entity the current form should operate upon.
   *
   * @return $this
   */
  public function setEntity(EntityPublishedInterface $entity) {
    $this->entity = $entity;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntityPublishedInterface $entity = NULL, $revision_log_message = TRUE) {
    if (!$entity) {
      return $form;
    }

    $this->setEntity($entity);
    CacheableMetadata::createFromObject($entity)->applyTo($form);

    $options = $entity->isPublished()
      ? ['unpublish' => $this->t('Unpublish')]
      : ['publish' => $this->t('Publish')];

    $form['action'] = [
      '#type' => 'select',
      '#title' => $this->t('Action'),
      '#options' => $options,
      '#disabled' => TRUE,
    ];

    $form['revision_log'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Revision log message'),
      '#description' => $this->t('Briefly describe the changes you have made.'),
      '#rows' => 2,
      '#access' => $revision_log_message,
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Apply'),
        '#button_type' => 'primary',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->entity->isPublished()) {
      $this->entity->setUnpublished();
    }
    else {
      $this->entity->setPublished();
    }

    if ($this->entity instanceof RevisionableInterface) {
      $this->entity->setNewRevision();

      if ($this->entity instanceof RevisionLogInterface) {
        $revision_log_message = $form_state->getValue('revision_log') ?: NULL;
        $this->entity->setRevisionLogMessage($revision_log_message);
      }
    }

    $this->entity->save();
  }

}
